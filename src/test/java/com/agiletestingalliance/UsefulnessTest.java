package com.agiletestingalliance;

import org.junit.Test;
import static org.junit.Assert.*;

public class UsefulnessTest {
	@Test
	public void testDesc() throws Exception {
		String res = new Usefulness().desc ();
		assertTrue(res.contains ("transformation"));
		
	}
	
	@Test
         public void testFunctionWF() throws Exception {
         	int number = new Usefulness().functionWF ();
	             assertEquals("testing return value", 5, 5) ;
	    }

}
