package com.agiletestingalliance;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
   public class MinMaxTest {
     @Test
      public void testBar() throws Exception {
         String k =new MinMax().bar("Vivek");
         assertEquals("Bar","Vivek",k);
      }
      @Test
      public void testBigger() throws Exception {
          int number =new MinMax().bigger(2,4);
          assertEquals("Bigger",4,number); 
      }
}
